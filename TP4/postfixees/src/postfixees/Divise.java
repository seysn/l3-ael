package postfixees;
public class Divise extends Operateur implements Yytoken {
 
	protected int calcul(int... values){
		return values[0] / values[1];
	}
  
	public Divise(String image){
		super(image, 2);
	}
}
