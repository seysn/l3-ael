package postfixees;
public class Fois extends Operateur implements Yytoken {
 
	protected int calcul(int... values){
		return values[0] * values[1];
	}
  
	public Fois(String image){
		super(image, 2);
	}
}
