package postfixees;

%%

%unicode
%line
%column

ENTIER_SIMPLE=[0-9]+
PLUS=[+]|plus
MOINS=[-]|minus
FOIS=[*]|mult
DIVISE=[/]|quo
LineTerminator = \r|\n|\r\n
WhiteSpace     = {LineTerminator} | [ \t\f]

%% 

{ENTIER_SIMPLE}
	{ return new Valeur(yytext()); }

{PLUS}
	{ return new Plus(yytext()); }

{MOINS}
	{ return new Moins(yytext()); }

{FOIS}
	{ return new Fois(yytext()); }

{DIVISE}
	{ return new Divise(yytext()); }

/* ajouter le cas des espaces et fins de ligne */
{WhiteSpace}
	{ }

/* ajouter les autres tokens */
