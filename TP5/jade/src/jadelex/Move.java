package jadelex;

import jade.Direction;

public class Move extends BaseToken {
    private Direction direction;
	
    public Direction getDirection(){
        return direction;
    }
	
    public Move(String d){
        super(TokenType.MOVE);
        switch(d) {
		case "nord":
			this.direction = Direction.NORTH;
			break;
		case "sud":
			this.direction = Direction.SOUTH;
			break;
		case "est":
			this.direction = Direction.EAST;
			break;
		case "ouest":
			this.direction = Direction.WEST;
			break;			
		}
    }
    public String toString(){
        return super.toString()+"["+direction+"]";
    }
}
