package jadelex;

%%

%class TokenizerV1
%implements Tokenizer
%public
%unicode
%line
%column

BAISSER=baisser
LEVER=lever
DIRECTION=nord|sud|est|ouest

UNKNOWN=[^\r\n]
LINETERMINATOR=\r|\n|\r\n
WHITESPACE={LINETERMINATOR} | [ \t\f]

COMMENT={SIMPLECOMMENT} | {MULTICOMMENT}
SIMPLECOMMENT="//"{UNKNOWN}*{LINETERMINATOR}?
MULTICOMMENT="/*" {COMMENTCONTENT} "*"+ "/"
COMMENTCONTENT=( [^*] | \*+ [^/*] )*

%%
	
{BAISSER}
	{ return new PenMode(true); }

{LEVER}
	{ return new PenMode(false); }

{DIRECTION}
	{ return new Move(yytext()); }

{WHITESPACE}
	{ }

{COMMENT}
	{ }

{MULTICOMMENT}
	{ }

{UNKNOWN}
	{ return new Unknown(yytext()); }
