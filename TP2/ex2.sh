#!/bin/sh

# ==========
# Exercice 2
# ==========

# Question 1

echo "\n==========\nQuestion 1\n==========\n"

valeurAttribut='="[^<"]*"'
egrep $valeurAttribut html/contact.html --color=auto

# Question 2

echo "\n==========\nQuestion 2\n==========\n"

nomXML='<[[:alnum:]]+[[:space:]]?'
refEntite=''

egrep $nomXML html/contact.html --color=auto
