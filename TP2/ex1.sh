#!/bin/sh

# ==========
# Exercice 1
# ==========

# Question 1

echo "\n==========\nQuestion 1\n==========\n"

egrep nez Cyrano.txt --color=auto

# Question 2

echo "\n==========\nQuestion 2\n==========\n"

egrep '\([[:alnum:][:space:]]*\)' Cyrano.txt

# Question 3

echo "\n==========\nQuestion 3\n==========\n"

egrep '\<[[:alpha:]]{4}\>' Cyrano.txt --color=auto

# Question 4

echo "\n==========\nQuestion 4\n==========\n"

echo "La commande s'arrête une fois qu'elle a trouvé une fois l'occurence"

# Question 5

echo "\n==========\nQuestion 5\n==========\n"

egrep '^[[:alpha:]]+[[:space:]]:[[:space:]]' Cyrano.txt --color=auto
