package ard;

import java.io.Reader;

/**
 *
 * @author seysn
 */
public class MyArd extends Ard {

    public MyArd(Reader in) {
        super(in);
    }

    private String S() throws SyntaxException, ParserException {
        switch (current) {
            case 'a':
            case 'b':
            case 'c':
            case '(':
                // S -> ERS
                String res = "";
                String tmp = E();
                int r = R();
                if (r == -1) {
                    res = tmp;
                }
                for (int i = 0; i < r; i++) {
                    res += tmp;
                }
                res += S();
                return res;
            case ')':
            case END_MARKER:
                // S -> epsilon
                return "";
            default:
                // Erreur
                throw new SyntaxException(ErrorType.NO_RULE, current);
        }
    }

    private String E() throws SyntaxException, ParserException {
        switch (current) {
            case 'a':
            case 'b':
            case 'c':
                // E -> L
                return L();
            case '(':
                // E -> (S)
                eat('(');
                String tmp = S();
                eat(')');
                return tmp;
            default:
                // Erreur
                throw new SyntaxException(ErrorType.NO_RULE, current);
        }
    }

    private int R() throws SyntaxException, ParserException {
        switch (current) {
            case 'a':
            case 'b':
            case 'c':
            case '(':
            case ')':
            case END_MARKER:
                // R -> epsilon
                return -1;
            case '0':
            case '1':
            case '2':
            case '3':
            case '4':
            case '5':
            case '6':
            case '7':
            case '8':
            case '9':
                // R -> C
                return C();
            default:
                // Erreur
                throw new SyntaxException(ErrorType.NO_RULE, current);
        }
    }

    private String L() throws SyntaxException, ParserException {
        switch (current) {
            case 'a':
            case 'b':
            case 'c':
                String res = "" + current;
                eat(current);
                return "" + res;
            default:
                // Erreur
                throw new SyntaxException(ErrorType.NO_RULE, current);
        }
    }

    private int C() throws SyntaxException, ParserException {
        if (current >= '0' && current <= '9') {
            String res = "" + current;
            eat(current);
            return Integer.parseInt("" + res);
        }
        throw new SyntaxException(ErrorType.NO_RULE, current);
    }

    @Override
    protected void axiom() throws SyntaxException, ParserException {
        System.out.println(S());
    }
}
