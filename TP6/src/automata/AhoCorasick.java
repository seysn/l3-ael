package automata;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author seysn
 */
public class AhoCorasick extends AbstractNDAutomaton {

    private List<String> words;
    private List<List<State>> branch;
    private Map<State, State> repli;
    private State root;

    public AhoCorasick(String... values) {
        super();
        words = new ArrayList<>(Arrays.asList(values));
        branch = new ArrayList<>();
        repli = new HashMap<>();
        createAutomaton();
        completerAutomate();
    }

    private void createAutomaton() {
        State q = null;

        // On recupere la taille maximum des mots
        int maxlength = 0;
        for (String s : words) {
            if (maxlength < s.length()) {
                maxlength = s.length();
            }
        }

        root = addNewState("root");
        setInitial(root);

        // Initialisation des branches et ajout de l'état root
        for (int i = 0; i < words.size(); i++) {
            branch.add(new ArrayList<>());
            branch.get(i).add(root);
        }

        for (int l = 0; l < maxlength; l++) {
            for (int i = 0; i < words.size(); i++) {
                if (l < words.get(i).length()) {
                    try {
                        // On crée un nouvel état avec comme nom les
                        // (l+1) premieres lettres du mot
                        //q = addNewState(words.get(i).substring(0, l + 1));
                        q = createNewState(branch.get(i).get(l), words.get(i).charAt(l));
                    } catch (StateException e) {
                        // Si cet état existe déjà, on l'ajoute à la branche du mot
                        for (State s : getStates()) {
                            if (s.getName().equals(words.get(i).substring(0, l + 1))) {
                                branch.get(i).add(s);
                            }
                        }
                        continue;
                    }

                    // On ajoute l'état puis on forme la transition
                    branch.get(i).add(q);
                    //addTransition(branch.get(i).get(l), words.get(i).charAt(l), q);

                    // Si les (l+1) premieres lettres forment le mot complet, on a fini
                    if (l == words.get(i).length() - 1) {
                        setAccepting(q);
                    }
                }
            }
        }
    }

    private State createNewState(State parent, char letter) {
        State q = addNewState(parent == root ? "" + letter : parent.getName() + letter);
        State s, e;
        addTransition(parent, letter, q);

        if (parent == root) {
            repli.put(q, parent);
        } else {
            s = parent;
            do {
                s = repli.get(s);
                e = null;
                for (State st : getTransitionSet(s, letter)) {
                    e = st;
                }
            } while (e != null || s != null && s == root);

            if (e != null) {
                repli.put(q, e);
                if (isAccepting(e)) {
                    setAccepting(q);
                }
            } else {
                repli.put(q, e);
            }
        }

        return q;
    }

    private void completerAutomate() {
        for (State st : states) {
            for (char c : alphabet) {
                if (getTransitionSet(st, c).isEmpty()) {
                    if (st == root) {
                        addTransition(st, c, root);
                    } else {
                        Set<State> set = getTransitionSet(repli.get(st), c);
                        for (State s : set) {
                            addTransition(st, c, s);
                            break;
                        }
                    }
                }
            }
        }
    }

    @Override
    public boolean accept(String word) throws StateException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String toString() {
        String s = super.toString();
        s += "\n";
        s += "Etats de repli :\n";
        for (State st : states) {
            State e = repli.get(st);
            s += st + " -> " + e + "\n";
        }
        return s;
    }
}
