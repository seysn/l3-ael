package automata.tests;

import automata.Automaton;
import automata.AutomatonBuilder;
import automata.NDAutomaton;
import automata.NDAutomatonWithDeterminisation;
import automata.StateException;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

public class TestND2 {

    /*
     * Écriture de la représentation graphviz de l'automate dans un fichier
     */
    private static void dotToFile(Automaton a, String fileName) {
        File f = new File(fileName);
        try {
            PrintWriter sortieDot = new PrintWriter(f);
            sortieDot.println(a.toGraphviz());
            sortieDot.close();
        } catch (IOException e) {
            System.out.println("création du fichier " + fileName + " impossible");
        }
    }
    
    public static void main(String[] args) throws StateException {
        AutomatonBuilder a = new NDAutomatonWithDeterminisation();
        a.addNewState("ini");
        a.addNewState("a");
        a.addNewState("b");
        a.addNewState("c");
        a.addNewState("bc");
        a.addNewState("ca");
        a.setInitial("ini");
        a.setAccepting("a");
        a.setAccepting("bc");
        a.setAccepting("ca");
        a.addTransition("ini", 'a', "a");
        a.addTransition("ini", 'b', "b");
        a.addTransition("ini", 'c', "c");
        a.addTransition("b", 'c', "bc");
        a.addTransition("c", 'a', "ca");
        a.addTransition("ini", 'a', "ini");
        a.addTransition("ini", 'b', "ini");
        a.addTransition("ini", 'c', "ini");

	//a.addTransition("q3", '0', "q3");
        //a.addTransition("q3", '1', "q3");
//        System.out.println(a.accept("01"));
//        System.out.println(a.accept("00"));

//        System.out.println(a.toGraphviz());
//        System.out.println(a);

        dotToFile(a, "automate-before.dot");
        NDAutomatonWithDeterminisation source = (NDAutomatonWithDeterminisation) a;
        Automaton d = source.deterministic(new NDAutomaton());
//        System.out.println(d);
//
//        System.out.println(d.toGraphviz());
        dotToFile(d, "automate-after.dot");

    }

}
