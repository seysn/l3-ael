package automata.tests;

import java.util.Collections;
import java.util.HashSet;

import automata.Automaton;
import automata.AutomatonBuilder;
import automata.NDAutomaton;
import automata.NDAutomatonWithDeterminisation;
import automata.StateException;

public class TestND3 {

    public static void main(String[] args) throws StateException {
        AutomatonBuilder a = new NDAutomatonWithDeterminisation();
        a.addNewState("q0");
        a.addNewState("q1");
        a.addNewState("q2");
        a.addNewState("q3");
        a.setInitial("q0");
        a.setAccepting("q2");
        a.setAccepting("q3");
        a.addTransition("q0", 'a', "q0");
        a.addTransition("q0", 'b', "q0");
        a.addTransition("q0", 'a', "q1");
        a.addTransition("q1", 'a', "q2");
        a.addTransition("q1", 'b', "q3");
        a.addTransition("q2", 'a', "q2");
        a.addTransition("q2", 'b', "q2");
        a.addTransition("q3", 'a', "q3");
        a.addTransition("q3", 'b', "q3");
	//a.addTransition("q3", 'a', "q3");
        //a.addTransition("q3", 'b', "q3");
        System.out.println(a.accept("01"));
        System.out.println(a.accept("00"));

        System.out.println(a.toGraphviz());
        System.out.println(a);

        NDAutomatonWithDeterminisation source = (NDAutomatonWithDeterminisation) a;
        Automaton d = source.deterministic(new NDAutomaton());
        System.out.println(d);

        System.out.println(d.toGraphviz());

    }

}
