package automata.tests;

import automata.Automaton;
import automata.AutomatonBuilder;
import automata.NDAutomaton;
import automata.NDAutomatonWithDeterminisation;
import automata.StateException;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

public class TestDeterminisation {

    /*
     * Écriture de la représentation graphviz de l'automate dans un fichier
     */
    private static void dotToFile(Automaton a, String fileName) {
        File f = new File(fileName);
        try {
            PrintWriter sortieDot = new PrintWriter(f);
            sortieDot.println(a.toGraphviz());
            sortieDot.close();
        } catch (IOException e) {
            System.out.println("création du fichier " + fileName + " impossible");
        }
    }
    
    public static void main(String[] args) throws StateException {
        AutomatonBuilder a = new NDAutomatonWithDeterminisation();
        a.addNewState("q1");
        a.addNewState("q2");
        a.addNewState("q3");
        a.addNewState("q4");
        a.addNewState("q5");
        a.setInitial("q1");
        a.setAccepting("q3");
        a.setAccepting("q4");
        a.addTransition("q1", 'a', "q2");
        a.addTransition("q2", 'b', "q3");
        a.addTransition("q3", 'a', "q3");
        a.addTransition("q2", 'b', "q4");
        a.addTransition("q4", 'a', "q5");
        a.addTransition("q5", 'b', "q4");

//        System.out.println(a.toGraphviz());
//        System.out.println(a);

        dotToFile(a, "automate-before.dot");
        NDAutomatonWithDeterminisation source = (NDAutomatonWithDeterminisation) a;
        Automaton d = source.deterministic(new NDAutomaton());
//        System.out.println(d);
//
//        System.out.println(d.toGraphviz());
        dotToFile(d, "automate-after.dot");

    }

}
