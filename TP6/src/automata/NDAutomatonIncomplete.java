package automata;

/**
 *
 * Implémentation d'un automate non déterministe. Version incomplète.
 *
 * @author Bruno.Bogaert (at) univ-lille1.fr
 *
 */
public class NDAutomatonIncomplete extends AbstractNDAutomaton implements Recognizer, AutomatonBuilder {

    @Override
    /**
     * Fake implementation : always return false.
     */
    public boolean accept(String word) {
        //  Ceci n'est pas une implémentation.
        return false;
    }

}
