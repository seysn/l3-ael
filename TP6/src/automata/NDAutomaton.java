package automata;

import java.util.HashSet;
import java.util.Set;

public class NDAutomaton extends AbstractNDAutomaton implements Recognizer, AutomatonBuilder {

    @Override
    public boolean accept(String word) {
        Set<State> s = new HashSet<>();
        s.addAll(initialStates);

        for (int i = 0; i < word.length(); i++) {
            s = getTransitionSet(s, word.charAt(i));
        }

        for (State s1 : s) {
            if (acceptingStates.contains(s1)) {
                return true;
            }
        }

        return false;
    }

    protected Set<State> getTransitionSet(Set<State> startSet, char letter) {
        Set<State> result = new PrintSet<>();

        for (State s : startSet) {
            result.addAll(s.getAutomaton().getTransitionSet(s, letter));
        }

        return result;
    }
}
