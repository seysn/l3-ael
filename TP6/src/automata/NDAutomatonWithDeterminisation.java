package automata;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;

public class NDAutomatonWithDeterminisation extends AbstractNDAutomaton implements Recognizer, AutomatonBuilder {

    @Override
    public boolean accept(String word) {
        Set<State> s = new PrintSet<>();
        s.addAll(initialStates);

        for (int i = 0; i < word.length(); i++) {
            s = getTransitionSet(s, word.charAt(i));
        }

        for (State s1 : s) {
            if (acceptingStates.contains(s1)) {
                return true;
            }
        }

        return false;
    }

    protected Set<State> getTransitionSet(Set<State> startSet, char letter) {
        Set<State> result = new PrintSet<>();
        
        for (State s : startSet) {
            result.addAll(s.getAutomaton().getTransitionSet(s, letter));
        }
        
        return result;
    }

    /**
     * Construit un automate déterministe et complet équivalent
     *
     * cible est supposé être un automate initialement vide cible va recevoir
     * l’automate déterministe créé
     *
     * @return déterminisé de l’automate (le résultat est l’objet reçu en
     * paramètre)
     */
    public Automaton deterministic(AutomatonBuilder cible) {
        Queue<Set<State>> qs = new LinkedList<>();
        Set<Set<State>> addedStates = new PrintSet<>();
        Set<State> currState = new PrintSet<>();
        Set<State> stmp = new PrintSet<>();
        
        /* Ajoute les états initiaux */
        for (State st : initialStates) {
            stmp.clear();
            stmp.add(st);
            qs.add(stmp);
            cible.addNewState("{" + st.getName() + "}");
            cible.setInitial(st);
        }
        
        do {
            currState = qs.poll();
            for (Character c : usedAlphabet()) {
                stmp = getTransitionSet(currState, c);
                if (!addedStates.contains(stmp)) {
                    addedStates.add(stmp);
                    cible.addNewState(stmp.toString());
                    qs.add(stmp);
                    for (State state : stmp) {
                        if (getAcceptingStates().contains(state)) {
                            cible.setAccepting(stmp.toString());
                            break;
                        }                            
                    }
                }
                cible.addTransition(currState.toString(), c, stmp.toString());
            }
        } while (!qs.isEmpty());
        
        return cible;
    }
}
